package main

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	"github.com/BurntSushi/toml"
)

type ConfigStruct struct {
	Subjects     []string
	ListingsFile string
}

var sampleConfig = ConfigStruct{
	Subjects:     make([]string, 0),
	ListingsFile: "files/listings.toml",
}

func (c *ConfigStruct) hasSubject(subject string) bool {
	for _, s := range c.Subjects {
		if s == subject {
			return true
		}
	}
	return false
}

var config ConfigStruct

// LoadConfig tries to load a toml configuration from the given file name,
// if there is no configuration file, it will create a new one with the data
// in sampleConfig.
func loadConfig(filename string) error {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		config = sampleConfig

		var buf bytes.Buffer
		encoder := toml.NewEncoder(&buf)
		err := encoder.Encode(config)
		if err != nil {
			return err
		}

		err = ioutil.WriteFile(filename, []byte(buf.String()), 0644)
		if err != nil {
			return err
		}
		fmt.Println("There was no config file, so I created a new one.")
		return nil
	}
	if _, err := toml.Decode(string(data), &config); err != nil {
		return err
	}

	return nil
}

func main() {
	const DefaultPort = 8080
	portPtr := flag.String("port", strconv.Itoa(DefaultPort),
		"The port the server runs on")
	ipStrPtr := flag.String("ip", "0.0.0.0", "The ip the server runs on")
	configPath := flag.String("config", "config.toml", "The path of your configuration file")
	flag.Parse()

	http.HandleFunc("/static/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, r.URL.Path[1:])
	})

	err := loadConfig(*configPath)
	if err != nil {
		panic(err)
	}

	err = loadListings()
	if err != nil {
		panic(err)
	}

	RegisterAll()

	fmt.Printf("Starting server at port %s and IP %s\n", string(*portPtr), string(*ipStrPtr))
	log.Fatal(http.ListenAndServe(*ipStrPtr+":"+string(*portPtr), nil))
}

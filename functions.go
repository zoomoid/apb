package main

import (
	"html/template"
	"net/http"
	"strconv"
	"strings"
	"time"
)

var regFunctions = map[string]func(http.ResponseWriter, *http.Request) error{
	"/":           search,
	"/data":       data,
	"/delete":     del,
	"/dodelete":   doDel,
	"/search":     doSearch,
	"/addlisting": addListing,
	"/info":       info,
}

func errorClosure(f func(http.ResponseWriter, *http.Request) error) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		err := f(w, r)
		if err != nil {
			w.Write([]byte(err.Error() + "\nIf this is bad, please contact me at https://git.rwth-aachen.de/h/apb"))
		}
	}
}

func RegisterAll() {
	for key, value := range regFunctions {
		http.HandleFunc(key, errorClosure(value))
	}
}

func render(f string, args interface{}, w http.ResponseWriter, r *http.Request) error {
	tmpl, err := template.ParseFiles("templates/template.html", f)
	if err != nil {
		return err
	}
	return tmpl.ExecuteTemplate(w, "template", args)
}

func renderError(err string, w http.ResponseWriter, r *http.Request) error {
	return render("templates/userinputerror.html", map[string]string{
		"Title": "Fehler",
		"Error": err,
	}, w, r)
}

func search(w http.ResponseWriter, r *http.Request) error {
	if r.URL.Path != "/" {
		http.NotFound(w, r)
		return nil
	}
	args := map[string]interface{}{
		"Subjects": config.Subjects,
		"Title":    "Suche",
	}
	return render("templates/search.html", args, w, r)
}

func doSearch(w http.ResponseWriter, r *http.Request) error {
	err := r.ParseForm()
	if err != nil {
		return err
	}
	subj := r.FormValue("subject")
	tut := r.FormValue("tutorium")
	if !config.hasSubject(subj) {
		return renderError("Dieses Fach existiert nicht. Bitte nutze die Autocomplete-Funktion, "+
			"oder falls du Javascript ablehnst die Liste von akzeptierten Fächern.\n", w, r)
	}
	num, err := strconv.Atoi(tut)
	if err != nil {
		return renderError("Das Tutorium muss eine Integer-Zahl sein", w, r)
	}

	tutString := tutoriumToString(subj, num)
	results := listings[tutString]

	var resultTemplate string

	if len(results) == 0 {
		resultTemplate = "templates/result_none.html"
	} else {
		resultTemplate = "templates/result_some.html"
	}

	tmpl, err := template.ParseFiles("templates/template.html", "templates/found.html", resultTemplate)
	if err != nil {
		return err
	}
	return tmpl.ExecuteTemplate(w, "template", map[string]interface{}{
		"FoundListings": results,
		"Subject":       tutString,
		"Title":         "Resultate",
	})
}

func data(w http.ResponseWriter, r *http.Request) error {
	args := map[string]interface{}{
		"Title": "Datenschutzerklärung",
	}
	return render("templates/data.html", args, w, r)
}

func addListing(w http.ResponseWriter, r *http.Request) error {
	if r.Method != "POST" {
		return renderError("Falsche Methode ("+r.Method+", nicht POST)", w, r)
	}
	err := r.ParseForm()
	if err != nil {
		return err
	}
	email := r.FormValue("email")
	numStudentsStr := r.FormValue("num_students")
	message := r.FormValue("message")
	agreeStr := r.FormValue("agree")
	subject := r.FormValue("subject")
	if email == "" {
		return renderError("Email leer", w, r)
	}
	numStudents, err := strconv.Atoi(numStudentsStr)
	if err != nil {
		return renderError("Anzahl Studenten muss eine Integer-Zahl sein", w, r)
	}
	if numStudents < 1 {
		return renderError("Anzahl Studenten muss >= 1 sein", w, r)
	}
	if agreeStr != "on" {
		return renderError("Du musst den Bedingungen zustimmen", w, r)
	}
	if !config.hasSubject(strings.Split(subject, "::")[0]) {
		return renderError("Das angegebene Fach existiert nicht", w, r)
	}
	listing := Listing{email, numStudents, message, time.Now().Format("02.01.2006 um 15:04")}
	listings[subject] = append(listings[subject], listing)
	err = saveListings(config.ListingsFile)
	if err != nil {
		return err
	}

	http.Redirect(w, r, r.Referer(), http.StatusFound)
	return nil
}

func info(w http.ResponseWriter, r *http.Request) error {
	args := map[string]interface{}{
		"Title": "Info",
	}
	return render("templates/info.html", args, w, r)
}

func del(w http.ResponseWriter, r *http.Request) error {
	args := map[string]interface{}{
		"Title": "Daten löschen",
	}
	return render("templates/delete.html", args, w, r)
}

func doDel(w http.ResponseWriter, r *http.Request) error {
	if r.Method != "POST" {
		return renderError("Method falsch ("+r.Method+" statt POST)", w, r)
	}
	err := r.ParseForm()
	if err != nil {
		return err
	}
	email := r.FormValue("email")
	if email == "" {
		return renderError("Email leer", w, r)
	}

	numDeleted := 0
	for key, value := range listings {
		i := 0
		for _, listing := range value {
			if strings.ToLower(listing.Email) != strings.ToLower(email) {
				value[i] = listing
				i++
			}
		}
		l := len(value)
		listings[key] = value[:i]
		numDeleted += l - i
	}

	err = saveListings(config.ListingsFile)
	if err != nil {
		return err
	}
	_, err = w.Write([]byte("Ok :)\nDeleted " + strconv.Itoa(numDeleted) + "\n"))
	return err
}

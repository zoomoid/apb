package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"strconv"

	"github.com/BurntSushi/toml"
)

func tutoriumToString(subj string, num int) string {
	return subj + "::" + strconv.Itoa(num)
}

type Listing struct {
	Email     string
	NumPeople int
	Message   string
	PostTime  string
}

type ListingsStruct map[string][]Listing

var listings ListingsStruct

// loadListings tries to load the listings from the file specified in the config.
// If this file does not exist, it will create a new one.
func loadListings() error {
	data, err := ioutil.ReadFile(config.ListingsFile)
	if err != nil {
		listings = ListingsStruct{}
		err = saveListings(config.ListingsFile)
		fmt.Println("There was no listings file, so I created a new one.")
		return err
	}
	if _, err := toml.Decode(string(data), &listings); err != nil {
		return err
	}
	return nil
}

func saveListings(filename string) error {
	var buf bytes.Buffer
	encoder := toml.NewEncoder(&buf)
	err := encoder.Encode(listings)
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(filename, []byte(buf.String()), 0644)
	if err != nil {
		return err
	}
	return nil
}

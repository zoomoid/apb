FROM golang:1.11

WORKDIR /go/src/apb/

COPY .  .

RUN go get -d -v ./...
RUN go install -v ./...

ENTRYPOINT ["apb"]
